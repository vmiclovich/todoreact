import React, { Component } from "react";
import SweetAlert from "sweetalert-react";
import Item from "./components/Item";
import 'sweetalert/dist/sweetalert.css';
import "./style.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      itemsDone: [],
      item: "",
      show: false
    };
  }
  // Where to learn more about form handling?
  // https://reactjs.org/docs/forms.html
  render() {
    return (
      <main>
        <h1>What should I do?</h1>
        <SweetAlert
          show={this.state.show}
          title="Warning!"
          text="Please type in your todo item"
          onConfirm={() => this.setState({ show: false })}
        />
        <form>
          <input
            id="item"
            type="text"
            placeholder="Item"
            value={this.state.item}
            onChange={e => this.setState({ item: e.target.value })}
          />
          <button
            className='btn'
            onClick={e => {
              e.preventDefault();
              if (this.state.item.trim() === "") {
                this.setState({ show: true });
              } else {
                this.setState({
                  items: [...this.state.items, this.state.item],
                  item: ""
                });
              }
            }}
          >
            Add
          </button>
        </form>
        <h3>Things to do</h3>
        {/* different ways of doing things */}
        <ul className="list">
          {/* What happens when there is no key in the iterator? */}
          {/* without proper management of "state", has someone clicked or not? Hard to know */}
          {/* {this.state.items.map((item, index) => <li key={index}><input type="checkbox" />{item}</li>)} */}

          {/* this is a dummy sample */}
          {/* <li> */}
          {/* <input type="checkbox" value={this.state.item} /> */}
          {/* <span className="strike">Strawberry</span> */}
          {/* </li> */}

          {/* More state management */}
          {this.state.items.map((item, index) => (
            <Item key={index} item={item} />
          ))}
        </ul>
      </main>
    );
  }
}

export default App;
