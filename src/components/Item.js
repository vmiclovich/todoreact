import React, { Component } from "react";

export default class Item extends Component {
  constructor(props) {
    super(props);
    this.state = {
      done: false
    };
  }
  //   components can have their logic too

  render() {
    //ref: https://reactjs.org/docs/faq-styling.html#how-do-i-add-css-classes-to-components
    let className = "";
    if (this.state.done) {
      className = "strike";
    }

    return (
      <li className={className}>
        <input
          type="checkbox"
          checked={this.state.done}
          onChange={() => this.setState({ done: !this.state.done })}
        />
        {this.props.item}
      </li>
    );
  }
}


// you can choose to export the class at the bottom too (or immediately at the top)
// export default Item;